/*
 * Provide core functionality for time based canvas visuals
 */

function JC_VisualCore()
{
	var myself = this;
	
	var mWidth =0;
	var mHeight = 0;
	var mCanvas;
	var mGraphics;
	var mDelay;
	var mIntervalRef;
	var mRender = function(_g,_w,_h)
	{
		alert("Render must be set!");
		myself.stop();
	};
	
	var mSetup = function(_w,_h)
	{
		alert("Setup must be set!");
		myself.stop();
	}
	
	this.setDelay = function(_delay)
	{
		mDelay = _delay;
	}
	
	this.registerCanvas = function(_canvasId)
	{
		mCanvas = document.getElementById(_canvasId);
		
		if(mCanvas.getContext)
		{
			mGraphics = mCanvas.getContext("2d");
			mWidth = parseInt(mCanvas.getAttribute("width"));
			mHeight = parseInt(mCanvas.getAttribute("height"));
			console.log("Width is " + mWidth);
			console.log("Height is " + mHeight);
			
			mSetup(mWidth,mHeight);
		}
		else
		{
			//browser does not support canvas
			console.log("Canvas not supported!");
		}
	}
	
	this.setRender = function(_render)
	{
		mRender = _render;
	}
	
	this.setSetup = function(_setup)
	{
		mSetup = _setup;
	}
	
	this.start = function()
	{
		console.log("Interval set to " + mDelay);
		mIntervalRef = setInterval(mRender,mDelay,mGraphics,mWidth,mHeight);
	}
	
	this.stop = function()
	{
		if(mIntervalRef)
		{
			clearInterval(mIntervalRef);
		}
	}
}
