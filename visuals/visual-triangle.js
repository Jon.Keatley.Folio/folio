/*
 * Creates a visual-core object and populates its details with bespoke values
 */

function JC_VisualTriangles()
{
	var mCore = new JC_VisualCore();
	
	//---
	
	var mPoints = [];
	var mTriangles = [];
	var mAngle = 0;
	var mDistance = 0.5;
	var mIsAlt = false;
	var mColors = [];
	var mBgIndex = 0;
	
	var mVol = 1;
	
	var newPoint = function(_x,_y)
	{
		return {"x":_x,"y":_y};
	}
	
	var debugPoint = function(_p)
	{
		console.log("Point X:" + _p.x + " Y:" + _p.y);
	}
	
	var splitTriangle = function(_t)
	{
		var cx = (_t[0].x + _t[1].x + _t[2].x) / 3;
		var cy = (_t[0].y + _t[1].y + _t[2].y) / 3;

		var t = [];
		
		for(var x=0;x<2;x++)
		{
			t.push([newPoint(_t[x].x,_t[x].y),
					newPoint(_t[x + 1].x,_t[x + 1].y),
					newPoint(cx,cy)]);		
		}
		
		t.push([newPoint(_t[2].x,_t[2].y),
				newPoint(_t[0].x,_t[0].y),
				newPoint(cx,cy)]);
				
		return t;
	}
	
	var Rnd = function(_min,_max)
	{
		return Math.floor(Math.random() * _max) + _min;
	}
	
	var AddOrReturnPointIndex = function(_p)
	{
		for(var x=0;x<mPoints.length;x++)
		{
			if(mPoints[x].x == _p.x && mPoints[x].y == _p.y)
			{
				return x;
			}
		}
		
		_p.vol = mVol;
		mVol += 0.25;
		
		return mPoints.push(_p) - 1;
	}
	
	var ToHexByte = function(_v)
	{
		var h = _v.toString(16);
		
		if(h.length === 1)
		{
			
			h = "0" + h;
		}
		
		return h;
	}
	
	//---
	
	mCore.setDelay(100);
	
	mCore.setSetup(function(_w,_h)
	{
		/*var step = 51;
		var r = 102;
		var b = 0;
		var g = 0;
		
		for(var y=0;y<4;y++)
		{
			for(var x=0;x<6;x++)
			{
				var color = ("#" + ToHexByte(r) + ToHexByte(b) + ToHexByte(g)).toUpperCase();
				mColors.push(color);
				g += step;
			}
			r += step;
			g = 0;
		}*/
		
		mColors.push("#fd7873");
		mColors.push("#ffa366");
		mColors.push("#ffce57");
		mColors.push("#309eff");
		mColors.push("#49c1fe");
		mColors.push("#61e5fe");
		mColors.push("#e860ff");
		mColors.push("#bb48ff");
		mColors.push("#ea60fe");
		
		mColors.push("#ff5f57");
		mColors.push("#ff804d");
		mColors.push("#ff732b");
		mColors.push("#2378f9");
		mColors.push("#37a8f6");
		mColors.push("#47d7f2");
		mColors.push("#6d1df8");
		mColors.push("#9a34f9");
		mColors.push("#c748f7");
		
		mColors.push("#fe4438");
		mColors.push("#fe5b32");
		mColors.push("#ff7129");
		mColors.push("#0061f6");
		mColors.push("#0189f5");
		mColors.push("#00b2f4");
		mColors.push("#5400f6");
		mColors.push("#7701f7");
		mColors.push("#9b00f4");
		
		var split = 4;
		var sw = _w / split;
		
		for(var x=0;x < split;x++)
		{
			var xo = sw * x;
			var xd = sw * (x + 1);
			var triangle = [newPoint(xo,0),
							newPoint(xd,_h),
							newPoint(xo,_h)];
			var tempTrangles = [];			
			var tbits = splitTriangle(triangle);			
			
			for(var y=0;y<tbits.length;y++)
			{
				tempTrangles.push(tbits[y]);
			}
			
			triangle = [newPoint(xd,0),
						newPoint(xd,_h),
						newPoint(xo,0)];
							
			tbits = splitTriangle(triangle);			
			
			for(var y=0;y<tbits.length;y++)
			{
				tempTrangles.push(tbits[y]);
			}
			
			//convert triangles to points
			for(var y=0;y<tempTrangles.length;y++)
			{
				var t = [];
				for(var z=0;z < 3;z++)
				{
					t.push(AddOrReturnPointIndex(tempTrangles[y][z]));
				}
				
				mTriangles.push(t);
			}
		}
	});
	
	mCore.setRender(function(_g,_w,_h)
	{
		_g.clearRect(0,0,_w,_h);
		
		_g.fillStyle="#20213d";

		for(var x=0;x<mPoints.length;x++)
		{
			mPoints[x].y += mPoints[x].vol;
			
			if(mPoints[x].y < 0)
			{
				mPoints[x].y = 0;
				mPoints[x].vol = -mPoints[x].vol;
			}
			else if(mPoints[x].y > _h)
			{
				mPoints[x].y = _h;
				mPoints[x].vol = -mPoints[x].vol;
			}
		}
		
		for(var x=0;x<mTriangles.length;x++)
		{
			_g.fillStyle= mColors[x];
			var t = mTriangles[x];
			_g.beginPath();
 
			var p = mPoints[mTriangles[x][0]];
			_g.moveTo(p.x,p.y);
			var p = mPoints[mTriangles[x][1]];
			_g.lineTo(p.x ,p.y );
			var p = mPoints[mTriangles[x][2]];
			_g.lineTo(p.x ,p.y );
			
			_g.closePath();
			_g.fill();
			_g.stroke();
		}
	});
	
	return mCore;
	
}
