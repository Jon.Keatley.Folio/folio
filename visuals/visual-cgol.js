/*
 * Creates a visual-core object and populates its details with bespoke values
 */

function JC_VisualCGOL()
{
	var mCore = new JC_VisualCore();
	
	//---
	
	var mWorld = [];
	var mWorldWidth = 140;
	var mWorldHeight = 35;
	
	function countNeighbours(_x,_y)
	{
		var count = 0;
		for(var cy = -1;cy <= 1;cy++)
		{
			for(var cx = -1;cx <= 1;cx++)
			{
				var nx = _x + cx;
				var ny = _y + cy;
				
				if(cy === 0 && cx === 0)
				{
					continue;
				}
				
				if(nx > 0 && ny > 0)
				{
					if(nx < mWorldWidth && ny < mWorldHeight)
					{
						if(mWorld[ny][nx] === 1)
						{
							count += 1;
						}	
					}
				}
			}
		}
		
		debug = false;
		
		return count;
	}
		
	function step()
	{
		var tmp = [];
		for(var y=0;y<mWorldHeight;y++)
		{
			tmp[y] = [];
			for(var x=0;x<mWorldWidth;x++)
			{
				var c = countNeighbours(x,y);
				
				if(mWorld[y][x] === 1)
				{
					if(c < 2)
					{
						tmp[y][x] = 0;
					}
					else if(c === 2 || c === 3)
					{
						tmp[y][x] = 1;
					}
					else
					{
						tmp[y][x] = 0;
					}
				}
				else if(c === 3)
				{
					tmp[y][x] = 1;
				}
				else
				{
					tmp[y][x] = 0;
				}
			}
		}
		
		mWorld = tmp;
	}
	
	function addBrickLayerOne(_y,_x)
	{
		mWorld[_y][_x]= 1;
		mWorld[_y][_x + 1]= 1;
		mWorld[_y][_x + 2]= 1;
		mWorld[_y][_x + 4]= 1;
		
		mWorld[_y + 1][_x]= 1;
		
		mWorld[_y + 2][_x + 3]= 1;
		mWorld[_y + 2][_x + 4]= 1;
		
		mWorld[_y + 3][_x + 1]= 1;
		mWorld[_y + 3][_x + 2]= 1;
		mWorld[_y + 3][_x + 4]= 1;
		
		mWorld[_y + 4][_x ] = 1;
		mWorld[_y + 4][_x + 2]= 1;
		mWorld[_y + 4][_x + 4]= 1;
	}
	
	function addGosperGlider(_y,_x)
	{
		var g = [];
		g[0] = [0,0,0,0,0 ,0,0,0,0,0, 0,0,0,0,0 ,0,0,0,0,0,0, 0,0,0,1,0 ,0,0,0,0,0 ,0,0,0,0,0];
		g[1] = [0,0,0,0,0 ,0,0,0,0,0, 0,0,0,0,0 ,0,0,0,0,0,0, 0,1,0,1,0 ,0,0,0,0,0 ,0,0,0,0,0];
		g[2] = [0,0,0,0,0 ,0,0,0,0,0, 0,0,1,1,0 ,0,0,0,0,0,1, 1,0,0,0,0 ,0,0,0,0,0 ,0,0,0,1,1];
		g[3] = [0,0,0,0,0 ,0,0,0,0,0, 0,1,0,0,0 ,1,0,0,0,0,1, 1,0,0,0,0 ,0,0,0,0,0 ,0,0,0,1,1];
		g[4] = [1,1,0,0,0 ,0,0,0,0,0, 1,0,0,0,0 ,0,1,0,0,0,1, 1,0,0,0,0 ,0,0,0,0,0 ,0,0,0,0,0];
		g[5] = [1,1,0,0,0 ,0,0,0,0,0, 1,0,0,0,1 ,0,1,1,0,0,0, 0,1,0,1,0 ,0,0,0,0,0 ,0,0,0,0,0];
		g[6] = [0,0,0,0,0 ,0,0,0,0,0, 1,0,0,0,0 ,0,1,0,0,0,0, 0,0,0,1,0 ,0,0,0,0,0 ,0,0,0,0,0];
		g[7] = [0,0,0,0,0 ,0,0,0,0,0, 0,1,0,0,0 ,1,0,0,0,0,0, 0,0,0,0,0 ,0,0,0,0,0 ,0,0,0,0,0];
		g[8] = [0,0,0,0,0 ,0,0,0,0,0, 0,0,1,1,0 ,0,0,0,0,0,0, 0,0,0,0,0 ,0,0,0,0,0 ,0,0,0,0,0];
		
		
		var ox = _x;
		var oy = _y;
		for(var y=0;y < g.length;y++)
		{
			for(var x=0;x < g[y].length;x++)
			{
				mWorld[oy + y][ox + x] = g[y][x];
			}
		}
		
		
	}
	
	function render(_g,_w,_h)
	{
		var cellWidth = _w / mWorldWidth;
		var cellHeight = _h / mWorldHeight;
		
		_g.fillStyle = "#FFFFFF";
		_g.fillRect(0,0,_w,_h);
		
		for(var y=0;y<mWorldHeight;y++)
		{
			for(var x=0;x<mWorldWidth;x++)
			{
				if(mWorld[y][x] === 1)
				{	
					var dx = x * cellWidth;
					var dy = y * cellHeight;
					
					//_g.fillStyle = "rgba(0, 255, 0, 0.4)";
					//_g.fillRect(dx - 2,dy - 2,cellWidth + 4,cellHeight + 4);
					
					_g.fillStyle = "rgba(0, 0, 0, 1)";
					_g.fillRect(dx + 1,dy + 1,cellWidth - 1,cellHeight - 1);
					//_g.fillRect(dx,dy,cellWidth,cellHeight);
				}
			}
		}
	}
	
	//---
	
	mCore.setDelay(250);
	
	mCore.setSetup(function(_w,_h)
	{
		//setup world
		for(var y=0;y<mWorldHeight;y++)
		{
			mWorld[y] = [];
			for(var x=0;x<mWorldWidth;x++)
			{
				mWorld[y][x] = 0;
			}
		}
		
		//addBrickLayerOne(py , px);
		
		var c = Math.round(mWorldWidth / 46);
		
		for(var x=0;x< c;x++)
		{
			addGosperGlider(2 ,(x * 46) + 1);
		}

	});
	
	mCore.setRender(function(_g,_w,_h)
	{
		render(_g,_w,_h);
		step();
	});
	
	return mCore;
	
}
