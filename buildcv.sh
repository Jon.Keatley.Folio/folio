#!/usr/bin/env bash

webstaterator build -w cv.json -o build/
cd build
pandoc -t html -o ../cv.pdf cv.md -ccv/cv-style.css \
--variable margin-left=1.5cm \
--variable margin-right=1.5cm \
--variable margin-top=1.5cm \
--variable margin-bottom=3.5cm \
--metadata pagetitle="Jon Keatley" \
--pdf-engine-opt=--enable-local-file-access
