""" script to generate a model.json file out of the skills, elements, and timeline json files """

import os
import json
import re
import math
from datetime import datetime
from datetime import date
from datetime import timedelta

FILE_ELEMENTS      = "elements.json"
FILE_SKILLS        = "skills.json"
FILE_INTRO         = "introduction.md"
FILE_INTRO_CV         = "introduction-cv.md"
FILE_SELECT_SKILLS = "select_skills.json"
REQUIRED_FILES = [
    FILE_ELEMENTS,
    FILE_SKILLS,
    FILE_INTRO,
    FILE_INTRO_CV,
    FILE_SELECT_SKILLS
]

DATE_FORMAT = "%Y-%m-%d"
YEAR_IN_DAYS = 365
ID_REGEX = "[^a-zA-Z0-9_]+"
ID_PRE_REPLACE = {
    "+":"p",
    "#":"sharp",
}
INTRO_YEARS_WORKED_TARGET = "XXX"

class DuplicateKeyException(Exception):
    """ Exception used to indecate an a key has been used more than once"""
    def __init__(self,msg,error = "Duplicate key found"):
        self.error = error
        super().__init__(msg)

def element_icons(timeline):
    for event in timeline:
        if event["type"] == "company":
            event["icon"] = "briefcase"
            event["icon2"] = "user"
        elif event["type"] == "education":
            event["icon"] = "award"
            event["icon2"] = "map-pin"
        else:
            event["icon"] = "circle"
            event["icon2"] = "square"

def skill_fact_last_used(skill, skill_data, timeline):
    found = False

    for event in timeline:
        for role in event["roles"]:
            if skill in role["skills"]:
                if event["active"]:
                    fact = f"Current used at {event['title']}"
                else:
                    fact = f"Last used at {event['title']}"

                skill_data["facts"].append(("map",fact))
                found = True
                break
        if found:
            break

    if not found:
        print(f"Skill {skill} is not linked to any roles")

def skill_fact_total_time_used(skill, skill_data, timeline):

    years = 0
    months = 0
    for event in timeline:
        if event["type"] == "education":
            continue

        for role in event["roles"]:
            if skill in role["skills"]:
                years += role["elapsed_years"]
                months += role["elapsed_months"]

    month_years = months // 12
    years += months // 12
    months -= (month_years * 12)

    span_txt = "Used for "
    if years > 0:
        span_txt += "{} year{} ".format(years,"s" if years > 1 else "")

    if months > 0:
        span_txt += "{} month{}".format(months,"s" if months > 1 else "")
    skill_data["facts"].append(("clock",span_txt))

def skill_fact_number_time_used(skill, skill_data, timeline):
    count = 0
    for event in timeline:
        for role in event["roles"]:
            if skill in role["skills"]:
                count += 1

    skill_data["facts"].append(("bar-chart",f"Used in {count} roles"))

def calculate_details(start_date, end_date):
    months = end_date.month - start_date.month
    months += 12*(end_date.year - start_date.year)
    years = months // 12
    months -= (years * 12)

    span_txt = ""
    if years > 0:
        span_txt = "{} year{} ".format(years,"s" if years > 1 else "")

    if months > 0:
        span_txt += "{} month{}".format(months,"s" if months > 1 else "")
    return span_txt, years, months

def calculate_elapsed_time(elements):
    """generates elapsed times for roles and total time per element """
    for event in elements:
        event_start = None
        event_end = None
        event_active = False

        for role in event["roles"]:
            span_txt = ""
            #calculate role elapsed time
            start = datetime.strptime(role["start"],DATE_FORMAT)
            if "end" not in role or len(role["end"]) == 0:
                end = datetime.now()
                role["active"] = True
                event_active = True
            else:
                end = datetime.strptime(role["end"],DATE_FORMAT)
                role["active"] = False

            span_txt, years, months = calculate_details(start,end)
            role["elapsed_txt"] = span_txt
            role["elapsed_years"] = years
            role["elapsed_months"] = months

            if event_start is None or event_start > start:
                event_start = start

            if event_end is None or event_end < end:
                event_end = end
        span_txt, years, months = calculate_details(event_start,event_end)
        event["start"] = event_start.strftime(DATE_FORMAT)
        if not event_active:
            event["end"] = event_end.strftime(DATE_FORMAT)
        event["elapsed_txt"] = span_txt
        event["elapsed_years"] = years
        event["elapsed_months"] = months
        event["active"] = event_active

def build_paths(path):
    """Locate and return file paths for all required files found"""
    found = {}
    for file in REQUIRED_FILES:
        target_path = os.path.join(path, file)
        if os.path.exists(target_path):
            found[file] = target_path

    return found

def generate_id(list_of_dict, target_key, regex):
    """Generate an id based on a regex and target_key"""
    map = {}
    for obj in list_of_dict:
        if target_key in obj:
            id = obj[target_key]
            for target, replace in ID_PRE_REPLACE.items():
                if target in id:
                    id = id.replace(target, replace)

            obj['id'] = regex.sub('',id)
            map[obj[target_key]] = obj['id']
        else:
            print(f"Warning!! Can't find {target_key} in {obj}")
    return map

def list_of_dict_to_dict(list_of_dict, target_key):
    """Converts a list of dicts to a dict using target_key from each item as a key"""
    result = dict()
    for obj in list_of_dict:
        if obj[target_key] in result:
            raise DuplicateKeyException("More than one object with {} of {} found".format(target_key, obj[target_key]))
        result[obj[target_key]] = obj
    return result

def update_element_skills(skill_map, elements):
    """Loops through all elements replacing skills using skill map"""
    for event in elements:
        for role in event["roles"]:
            mapped_skills = []
            for skill in role["skills"]:
                mapped_skills.append(skill_map[skill])
            role["skills"] = mapped_skills

def group_element_skills_by_type(elements, skills):
    """Loop through all elements and group skills by type"""
    for event in elements:
        for role in event["roles"]:
            grouped_skills = {}
            for skill in role["skills"]:
                if skills[skill]["type"] not in grouped_skills.keys():
                    grouped_skills[skills[skill]["type"]] = []
                grouped_skills[skills[skill]["type"]].append(skill)
            role["grouped_skills"] = grouped_skills

def generate():
    """ loads in all required files and converts them into a single json file with added facts"""
    print("Finding files")
    files = build_paths(os.getcwd())

    if len(files) < len(REQUIRED_FILES):
        print("Missing required files :-")
        for file in REQUIRED_FILES:
            if file not in files:
                print(file)
        os.exit(-1)

    print("Loading files")
    with open(files[FILE_ELEMENTS]) as elements_stream:
        elements = json.load(elements_stream)["elements"]

    with open(files[FILE_SKILLS]) as skills_stream:
        skills = json.load(skills_stream)["skills"]

    with open(files[FILE_INTRO]) as intro_stream:
        intro_txt = intro_stream.read()

    with open(files[FILE_INTRO_CV]) as intro_stream_cv:
        intro_txt_cv = intro_stream_cv.read()

    with open(files[FILE_SELECT_SKILLS]) as select_skills_stream:
        select_skills = json.load(select_skills_stream)["select_skills"]

    # add timeline facts
    calculate_elapsed_time(elements)

    # build skill ids
    id_reg_obj = re.compile(ID_REGEX)
    skill_map = generate_id(skills, "title", id_reg_obj)
    update_element_skills(skill_map, elements)
    select_skills = [skill_map[skill] for skill in select_skills]

    # build skill dict
    skill_dict = list_of_dict_to_dict(skills,"id")
    group_element_skills_by_type(elements, skill_dict)

    #order timeline
    model = dict()
    model["skills"] = skill_dict
    model["timeline"] = sorted(elements, key=lambda event: datetime.strptime(event["start"],DATE_FORMAT), reverse=True)

    #order roles
    for event in model["timeline"]:
        event["roles"] = sorted(event["roles"], key=lambda role: datetime.strptime(role["start"],DATE_FORMAT),reverse=True)

    #calculate time working
    total_years = 0
    total_months = 0
    for event in model["timeline"]:
        if event["type"] == "company":
            total_years += event["elapsed_years"]
            total_months += event["elapsed_months"]
    total_years = math.floor(total_years + total_months / 12)
    model["introduction"] = intro_txt.replace(INTRO_YEARS_WORKED_TARGET,f"{total_years}")
    model["introduction_cv"] = intro_txt_cv.replace(INTRO_YEARS_WORKED_TARGET,f"{total_years}")

    # add skill facts
    for skill, skill_data in skill_dict.items():
        skill_data["facts"] = []
        skill_fact_last_used(skill, skill_data, model["timeline"])
        skill_fact_total_time_used(skill, skill_data, model["timeline"])
        skill_fact_number_time_used(skill, skill_data, model["timeline"])

    #Collect select skills
    model["select_skills"] = [skill_dict[skill] for skill in select_skills]

    # add icons to elements
    element_icons(model["timeline"])

    #output results
    with open("model.json","w") as model_out:
        json.dump(model,model_out,indent=4)
    print("Done")


if __name__ == "__main__":
    generate()
