""" script to populate the skills enum of elements.schema.json with skills """

import os
import json

SKILLS_FILE = "skills.json"
ELEMENTS_SCHEMA = "elements.schema.json"
REQUIRED_FILES = [SKILLS_FILE, ELEMENTS_SCHEMA]

def build_paths(path):
    """Locate and return file paths for all required files found"""
    found = {}
    for file in REQUIRED_FILES:
        target_path = os.path.join(path, file)
        if os.path.exists(target_path):
            found[file] = target_path

    return found

def update_skills():
    """compiles a list of skills and then injects them into ELEMENTS_SCHEMA"""
    print("Finding files")
    files = build_paths(os.getcwd())

    if len(files) < len(REQUIRED_FILES):
        print("Missing required files :-")
        for file in REQUIRED_FILES:
            if file not in files:
                print(file)
        os.exit(-1)

    print("Loading skills")
    with open(files[SKILLS_FILE]) as skills_stream:
        skills = json.load(skills_stream)

    print("Loading schema")
    with open(files[ELEMENTS_SCHEMA]) as elements_stream:
        element_schema = json.load(elements_stream)

    # list skills
    skill_list = [skill["title"] for skill in skills["skills"]]

    print(f"Found {len(skill_list)} skills")

    #update elements enum
    #When there is time improve this
    target_enum = element_schema["properties"]["elements"]["items"]["properties"]["roles"]["items"]["properties"]["skills"]["items"]

    if "enum" in target_enum:
        print("Update target found")
        target_enum["enum"] = skill_list

    #output results
    with open(files[ELEMENTS_SCHEMA],"w") as schema_out:
        json.dump(element_schema,schema_out,indent=4)
    print("Done")


if __name__ == "__main__":
    update_skills()
