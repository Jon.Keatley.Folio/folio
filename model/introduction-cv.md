
# Jon Keatley
### Project Leader at EBI

**Leader** and **Software Engineer** with XXX years experience in a wide array of industries, utilising a diverse range of technologies. I currently lead a team focused on building the next iteration of the Ensembl web application.

More information can be found at [jon-keatley.com](https://jon-keatley.com)  

Examples of my work can be found at [gitlab.com/users/Jon.Keatley.Folio](https://gitlab.com/users/Jon.Keatley.Folio/projects)

The best way to contact me is by [recruitment@jon-keatley.com](mailto:recruitment@jon-keatley.com)
