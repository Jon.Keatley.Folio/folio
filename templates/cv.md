
{{ model["introduction_cv"] }}
<div class="columns">
<div class="main">
## Work History
{% for event in model["timeline"] if event["type"] == "company" %}
<div class="company-title">
<div>{{ event["title"] }}</div>
<div>{{ event["start"]|date_fmt("%b %Y") }} - {% if "end" in event %}{{ event["end"]|date_fmt("%b %Y") }}{% else %}Present{% endif %}</div>
</div>

{% if event["roles"]|length > 1 %}
<!--span style="float:right">{{ event["elapsed_txt"] }}</span-->
{% endif %}

{% set events_loop = loop %}
{% for role in event["roles"] %}

<div class="role-columns">
<div>{{ role["role"] }}</div><div>{{ role["elapsed_txt"] }}</div>
</div>

{{role["blurb"] }}
<div class="role-skill-lists">
<div>
{% if "language" in role["grouped_skills"] %}
<span class="little">Languages:</span>  
{% for skill in role["skills"] if model["skills"][skill]["type"] == "language" %}<span class="skill s_language">{{ model["skills"][skill]["title"] }}</span> {% endfor %}
{% endif %}
{% if events_loop.index <= 3 %}
{% if "framework" in role["grouped_skills"] %}
<span class="little">Frameworks:</span>   
{% for skill in role["skills"] if model["skills"][skill]["type"] == "framework" %}<span class="skill s_framework">{{ model["skills"][skill]["title"] }}</span> {% endfor %}
{% endif %}
</div>
<div>
{% if "markup" in role["grouped_skills"] %}
<span class="little">Markup:</span>  
{% for skill in role["skills"] if model["skills"][skill]["type"] == "markup" %}<span class="skill s_markup">{{ model["skills"][skill]["title"] }}</span> {% endfor %}
{% endif %}
{% if "knowledge" in role["grouped_skills"] %}
<span class="little">Tools & Techniques:</span>    
{% for skill in role["skills"] if model["skills"][skill]["type"] == "knowledge" %}<span class="skill s_know">{{ model["skills"][skill]["title"] }}</span> {% endfor %}
{% endif %}
{% endif %}
</div>
</div>
{% endfor %}
{% endfor %}
### Education
{% for event in model["timeline"] if event["type"] == "education" %}
<div class="education-columns">
<div>{{ event["title"] }}</div>
<div>{{ event["start"]|date_fmt("%b %Y") }} - {% if "end" in event %}{{ event["end"]|date_fmt("%b %Y") }}{% else %}Present{% endif %}</div>
</div>
{% for role in event["roles"] %}
<span class="award">{{ role["role"] }}</span>
{% endfor %}
{% endfor %}
</div>
<!--div  class="skills">
## Highlighted Skills
{% for skill in model["select_skills"] %}

**{{ skill["title"] }}**
{% for fact in skill["facts"] if fact[0] != "map" %}
- {{ fact[1] }}
{% endfor %}
{% endfor %}
</div-->
</div>
