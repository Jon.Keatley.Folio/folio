/*
 * Manages art asset switching based on webpages width. As a failsafe the 
 * highist quality art asset should be use as the image source and then switched
 * 
 * Registered images provide a source and a trigger width.
 * When the view width is altered if the change is greater than the 
 * defined trigger gap (default 50px) then a scan of all dynamic images is
 * done and their sources switched to highest trigger source provided.
 * 
 * Image should be given the a name that relates to provided by RegisterImageSet
 * 
 * image format
 * ------------
 * name
 * trigger/url
 * 
 */

function JC_ResponsiveImages()
{
	var myself = this;
	var mLastTriggerPoint = -1;
	var mTriggerGap = 25; //only trigger once width is 50 pixels different from last trigger point
	var mRegisteredImageSets = {};
	var mImages = [];
	var mHasLoaded = false;
	
	var findTargetImages = function()
	{
		//find all targetted images
		var imgs = document.images;
		console.log("Found " + imgs.length);
		for(var x=0;x<imgs.length;x++)
		{
			console.log("Checking " + imgs[x].name);
			if(mRegisteredImageSets[imgs[x].name] !== undefined)
			{
				console.log("Registered image found!");
				mImages.push(imgs[x]);
			}
		}
	}
	
	this.CreateImageObject = function(_src,_trigger)
	{
		return { "src":_src,"trigger":_trigger};
	}
	
	this.RegisterImageSet = function(_name,_imgs)
	{
		mRegisteredImageSets[_name] = _imgs;
		
		console.log(mRegisteredImageSets[_name].length);
		
		if(mHasLoaded)
		{
			findTargetImages();
		}
	}
	
	this.SetTriggerGap = function(_trigger)
	{
		mTriggerGap = _trigger;
	}
	
	var onResize = function()
	{
		//console.log(mLastTriggerPoint + " " + window.innerWidth);
		var gap = Math.abs(mLastTriggerPoint - window.innerWidth);
		
		if(gap >= mTriggerGap)
		{
			mLastTriggerPoint = window.innerWidth;
			
			//loop through images
			for(var x=0;x< mImages.length;x++)
			{
				var t = mRegisteredImageSets[mImages[x].name];
				var src = "";
				var trig = -1;
					
				//figure out correct source
				for(var y=0;y<t.length;y++)
				{
					if(t[y].trigger > trig && t[y].trigger <= mLastTriggerPoint)
					{
						trig = t[y].trigger;
						src = t[y].src;
						
						//console.log("Width " + mLastTriggerPoint + " Trig " + trig);
					}
				}
				
				//check if source already set
				if(src.length > 0)
				{
					if(mImages[x].src !== src)
					{
						var tmpImg = new Image();
						tmpImg.src = src;
						tmpImg.requester = mImages[x];
						tmpImg.onload = function()
						{
							this.requester.src = this.src;
						}
					}
				}
			}
		}
		
	}
	
	var onLoad = function()
	{
		//setup resize hook
		console.log("loading resize hook");
		_JC_INST_.PageEvents.RegisterEvent("onresize",onResize);
		
		mHasLoaded = true;
		
		findTargetImages();

		//update images to correct size
		onResize();
	}
	
	_JC_INST_.PageEvents.RegisterEvent("onload",onLoad);

	return this;
}

if(_JC_INST_ === undefined)
{
	var _JC_INST_ = {};
}
_JC_INST_.Images = JC_ResponsiveImages();
