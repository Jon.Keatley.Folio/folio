/*
 * Provides a hook for when elements scroll in and out of view
 */

function JC_ScrollHooks()
{
	var myself = this;
	var mTargets = new Array();
	
	if(typeof JC_PageEvents === 'undefined')
	{
		alert("ScrollHooks requires JC_PageEvents!");
		return;
	}
	
	//register hook
	_JC_INST_.PageEvents.RegisterEvent("onscroll",function()
	{
		var scrollTop = document.body.scrollTop;
		var scrollBottom = document.body.scrollBottom;
		
		for(var x=0;x<mTargets.length;x++)
		{
			var tar = mTargets[x];
			var tTop = tar.obj.offsetTop;
			var tBottom = tar.obj.offsetBottom;
			var tHeight = tar.obj.offsetHeight;
			
			if(tTop < scrollTop)
			{
				if(tar.status !== 1 || tar.status === -1)
				{
					tar.event(false);
				}
				
				tar.status = 1;
			}
			else if(tTop > scrollBottom)
			{
				if(tar.status !== 3 || tar.status === -1)
				{
					tar.event(false);
				}
				
				tar.status = 3;
			}
			else if(tar.status !== 2)
			{
				tar.status = 2;
				tar.event(true);
			}
		}
		
	});
	
	this.addScrollEvent = function(object,event)
	{
		mTargets.push({"obj":object,"event":event,"state":-1});
	}
}

if(_JC_INST_ === undefined)
{
	var _JC_INST_ = {};
}
_JC_INST_.ScrollHooks = new JC_ScrollHooks();
